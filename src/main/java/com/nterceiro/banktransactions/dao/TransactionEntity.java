package com.nterceiro.banktransactions.dao;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "transactions")
@Data
@NoArgsConstructor
public class TransactionEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;
    private String reference;
    @NonNull
    private String accountIban;
    private LocalDateTime date;
    @NonNull
    private Double amount;
    private Double fee;
    private String description;

}
