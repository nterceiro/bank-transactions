package com.nterceiro.banktransactions.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionsRepository extends CrudRepository<TransactionEntity, Long> {

    @Override
    List<TransactionEntity> findAll();

    TransactionEntity findByReference(String reference);

}
