package com.nterceiro.banktransactions.dao;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "accounts")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @NonNull
    private String accountIban;
    @NonNull
    private Double amount;
    private String clientName;

}
