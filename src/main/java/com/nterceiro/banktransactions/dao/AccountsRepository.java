package com.nterceiro.banktransactions.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountsRepository extends CrudRepository<AccountEntity, Long> {

    @Override
    List<AccountEntity> findAll();

    AccountEntity findByAccountIban(String accountIban);

}
