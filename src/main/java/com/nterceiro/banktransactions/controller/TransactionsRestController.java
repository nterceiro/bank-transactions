package com.nterceiro.banktransactions.controller;

import com.nterceiro.banktransactions.exception.TransactionException;
import com.nterceiro.banktransactions.model.*;
import com.nterceiro.banktransactions.service.AccountsService;
import com.nterceiro.banktransactions.service.TransactionsService;
import com.nterceiro.banktransactions.service.TransactionsStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/rest")
public class TransactionsRestController {

    public static final String SORT_EXCEPTION = "Not valid type of sorting";

    @Autowired
    private TransactionsService transactionsService;
    @Autowired
    private AccountsService accountsService;
    @Autowired
    private TransactionsStatusService statusService;

    @PostMapping(value = "/transaction", consumes = "application/json")
    public void createTransaction(@RequestBody Transaction transaction) {
        try {
            transactionsService.applyTransaction(transaction);
        } catch (TransactionException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/transactions")
    public List<Transaction> findAllTransactions(@RequestParam(required = false) String iban,
                                                 @RequestParam(required = false, defaultValue = "NONE") String sort) {
        try {
            Sort sortedBy = Sort.valueOf(sort);
            return transactionsService.findTransactions(iban, sortedBy);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, SORT_EXCEPTION);
        }
    }

    @PostMapping(value = "/status", consumes = "application/json")
    public TransactionResponse checkStatus(@RequestBody TransactionStatus transactionStatus) {
        return statusService.checkStatus(transactionStatus);
    }

    @GetMapping("/accounts")
    public List<Account> findAllAccounts() {
        return accountsService.findAll();
    }

}
