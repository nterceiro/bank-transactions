package com.nterceiro.banktransactions.model;

public enum Status {
    PENDING, SETTLED, FUTURE, INVALID
}
