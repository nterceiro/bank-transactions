package com.nterceiro.banktransactions.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransactionResponse {

    @NonNull
    private String reference;
    @NonNull
    private Status status;
    private Double amount;
    private Double fee;

}
