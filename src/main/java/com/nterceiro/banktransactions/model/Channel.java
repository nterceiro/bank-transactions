package com.nterceiro.banktransactions.model;

public enum Channel {
    CLIENT, ATM, INTERNAL
}
