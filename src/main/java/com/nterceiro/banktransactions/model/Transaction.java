package com.nterceiro.banktransactions.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Transaction {

    private String reference;
    @NonNull
    @JsonProperty("account_iban")
    private String accountIban;
    private LocalDateTime date;
    @NonNull
    private Double amount;
    private Double fee;
    private String description;
}
