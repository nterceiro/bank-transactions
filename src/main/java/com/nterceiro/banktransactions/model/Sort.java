package com.nterceiro.banktransactions.model;

public enum Sort {
    NONE, ASCENDING, DESCENDING
}
