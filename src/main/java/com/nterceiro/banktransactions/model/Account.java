package com.nterceiro.banktransactions.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Account {

    @NonNull
    private String accountIban;
    @NonNull
    private Double amount;
    private String clientName;

}
