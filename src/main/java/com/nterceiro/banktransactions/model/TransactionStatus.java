package com.nterceiro.banktransactions.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
public class TransactionStatus {

    @NonNull
    private String reference;
    private Channel channel;
}
