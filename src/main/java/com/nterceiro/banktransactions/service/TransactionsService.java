package com.nterceiro.banktransactions.service;

import com.nterceiro.banktransactions.dao.AccountEntity;
import com.nterceiro.banktransactions.dao.AccountsRepository;
import com.nterceiro.banktransactions.dao.TransactionEntity;
import com.nterceiro.banktransactions.dao.TransactionsRepository;
import com.nterceiro.banktransactions.exception.TransactionException;
import com.nterceiro.banktransactions.model.Sort;
import com.nterceiro.banktransactions.model.Transaction;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class TransactionsService {

    public static final String AMOUNT_EXCEPTION = "Not enough credit for this transaction";

    private TransactionsRepository transactionsRepository;
    private AccountsRepository accountsRepository;
    private ModelMapper modelMapper;

    public TransactionsService(@Autowired TransactionsRepository transactionsRepository,
                               @Autowired AccountsRepository accountsRepository,
                               @Autowired ModelMapper modelMapper) {
        this.transactionsRepository = transactionsRepository;
        this.accountsRepository = accountsRepository;
        this.modelMapper = modelMapper;
    }

    public Transaction save(Transaction transaction) {
        if (transaction.getReference() == null) {
            transaction.setReference(UUID.randomUUID().toString());
        }
        TransactionEntity entity = modelMapper.map(transaction, TransactionEntity.class);
        transactionsRepository.save(entity);
        return transaction;
    }

    @Transactional
    public void applyTransaction(Transaction transaction) throws TransactionException {
        AccountEntity account = findOrOpenAccount(transaction.getAccountIban());
        double totalAmount = calculateTotalAmount(transaction);
        if (account.getAmount() + totalAmount < 0)
            throw new TransactionException(AMOUNT_EXCEPTION);

        TransactionEntity transactionEntity = modelMapper.map(transaction, TransactionEntity.class);
        account.setAmount(account.getAmount() + totalAmount);
        accountsRepository.save(account);
        transactionsRepository.save(transactionEntity);
    }

    public List<Transaction> findTransactions(@Nullable String iban, Sort sort) {
        List<TransactionEntity> filteredTransaction = transactionsRepository.findAll().stream()
                .filter(tr -> iban == null || iban.equalsIgnoreCase(tr.getAccountIban()))
                .collect(Collectors.toList());
        switch (sort) {
            case ASCENDING:
                filteredTransaction.sort(Comparator.comparing(TransactionEntity::getAmount));
                break;
            case DESCENDING:
                filteredTransaction.sort(Comparator.comparing(TransactionEntity::getAmount));
                Collections.reverse(filteredTransaction);
                break;
            default:
        }
        return filteredTransaction.stream()
                .map(te -> modelMapper.map(te, Transaction.class))
                .collect(Collectors.toList());
    }

    private AccountEntity findOrOpenAccount(String accountIban) {
        AccountEntity account = accountsRepository.findByAccountIban(accountIban);
        if (account == null) {
            account = AccountEntity.builder()
                    .accountIban(accountIban)
                    .amount(0.0)
                    .build();
        }
        return account;
    }

    private double calculateTotalAmount(Transaction transaction) {
        return transaction.getFee() == null ?
                transaction.getAmount() :
                transaction.getAmount() - transaction.getFee();
    }

}
