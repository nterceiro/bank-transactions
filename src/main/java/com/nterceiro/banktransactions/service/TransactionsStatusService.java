package com.nterceiro.banktransactions.service;

import com.nterceiro.banktransactions.dao.TransactionEntity;
import com.nterceiro.banktransactions.dao.TransactionsRepository;
import com.nterceiro.banktransactions.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static com.nterceiro.banktransactions.model.Channel.ATM;
import static com.nterceiro.banktransactions.model.Channel.INTERNAL;
import static com.nterceiro.banktransactions.model.Status.*;

@Service
public class TransactionsStatusService {

    private TransactionsRepository transactionsRepository;

    public TransactionsStatusService(@Autowired TransactionsRepository transactionsRepository) {
        this.transactionsRepository = transactionsRepository;
    }

    public TransactionResponse checkStatus(TransactionStatus transactionStatus) {
        TransactionEntity transaction = transactionsRepository.findByReference(transactionStatus.getReference());

        if (transaction == null || transaction.getDate() == null) {
            return TransactionResponse.builder()
                    .reference(transactionStatus.getReference())
                    .status(INVALID)
                    .build();
        } else {
            return buildTransactionResponse(transaction, transactionStatus.getChannel());
        }
    }

    private TransactionResponse buildTransactionResponse(TransactionEntity transaction, Channel channel) {
        Status status = buildStatus(transaction.getDate(), channel);

        return channel == INTERNAL ? buildCompleteTransactionResponse(transaction, status)
                : buildCalculatedTransactionResponse(transaction, status);
    }

    private Status buildStatus(LocalDateTime localDateTime, Channel channel) {
        if (localDateTime.toLocalDate().isBefore(LocalDate.now()))
            return SETTLED;
        else if (localDateTime.toLocalDate().isAfter(LocalDate.now()))
            return channel == ATM ? PENDING : FUTURE;
        else
            return PENDING;
    }

    private TransactionResponse buildCompleteTransactionResponse(TransactionEntity transaction, Status status) {
        double fee = transaction.getFee() == null ? 0.0 : transaction.getFee();
        return TransactionResponse.builder()
                .reference(transaction.getReference())
                .status(status)
                .amount(transaction.getAmount())
                .fee(fee)
                .build();
    }

    private TransactionResponse buildCalculatedTransactionResponse(TransactionEntity transaction, Status status) {
        double fee = transaction.getFee() == null ? 0.0 : transaction.getFee();
        return TransactionResponse.builder()
                .reference(transaction.getReference())
                .status(status)
                .amount(transaction.getAmount() - fee)
                .build();
    }

}
