package com.nterceiro.banktransactions.service;

import com.nterceiro.banktransactions.dao.AccountsRepository;
import com.nterceiro.banktransactions.model.Account;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccountsService {

    private AccountsRepository accountsRepository;
    private ModelMapper modelMapper;

    public AccountsService(@Autowired AccountsRepository accountsRepository,
                           @Autowired ModelMapper modelMapper) {
        this.accountsRepository = accountsRepository;
        this.modelMapper = modelMapper;
    }

    public List<Account> findAll() {
        return accountsRepository.findAll().stream()
                .map(ae -> modelMapper.map(ae, Account.class))
                .collect(Collectors.toList());
    }

}
