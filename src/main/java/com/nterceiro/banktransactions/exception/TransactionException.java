package com.nterceiro.banktransactions.exception;

public class TransactionException extends Exception {

    public TransactionException(String msg) {
        super(msg);
    }
}
