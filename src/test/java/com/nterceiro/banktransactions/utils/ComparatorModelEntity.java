package com.nterceiro.banktransactions.utils;

import com.nterceiro.banktransactions.dao.TransactionEntity;
import com.nterceiro.banktransactions.model.Transaction;

public class ComparatorModelEntity {

    public static boolean assertEqualWithoutId(Transaction model, TransactionEntity entity) {
        return compareNullableField(model.getReference(), entity.getReference())
                && model.getAccountIban().equals(entity.getAccountIban())
                && compareNullableField(model.getDate(), entity.getDate())
                && model.getAmount().equals(entity.getAmount())
                && compareNullableField(model.getFee(), entity.getFee())
                && compareNullableField(model.getDescription(), entity.getDescription());
    }

    private static boolean compareNullableField(Object o1, Object o2) {
        return o1 == null
                ? o2 == null
                : o2 != null && o1.equals(o2);
    }

}
