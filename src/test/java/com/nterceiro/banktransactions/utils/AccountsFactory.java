package com.nterceiro.banktransactions.utils;

import com.nterceiro.banktransactions.model.Account;

public class AccountsFactory {

    public static final Double INITIAL_CASH = 200.0;
    public static final String ACCOUNT_WITH_CASH = "ES123456";
    public static final String ACCOUNT_WITHOUT_CASH = "ES345678";

    public static Account buildAccountWithCash() {
        Account account = new Account();
        account.setAccountIban(ACCOUNT_WITH_CASH);
        account.setAmount(INITIAL_CASH);
        account.setClientName("Client One");
        return account;
    }

    public static Account buildAccountWithoutCash() {
        Account account = new Account();
        account.setAccountIban(ACCOUNT_WITHOUT_CASH);
        account.setAmount(0.0);
        account.setClientName("Client One");
        return account;
    }
}
