package com.nterceiro.banktransactions.utils;

import com.nterceiro.banktransactions.model.Channel;
import com.nterceiro.banktransactions.model.TransactionStatus;

import static com.nterceiro.banktransactions.model.Channel.CLIENT;
import static com.nterceiro.banktransactions.utils.TransactionsFactory.DEFAULT_REFERENCE;
import static com.nterceiro.banktransactions.utils.TransactionsFactory.NOT_REGISTERED_REFERENCE;

public class TransactionStatusFactory {

    public static TransactionStatus buildInvalidTransactionStatus() {
        TransactionStatus transactionStatus = new TransactionStatus();
        transactionStatus.setReference(NOT_REGISTERED_REFERENCE);
        transactionStatus.setChannel(CLIENT);
        return transactionStatus;
    }

    public static TransactionStatus buildValidTransactionStatus(Channel channel) {
        TransactionStatus transactionStatus = new TransactionStatus();
        transactionStatus.setReference(DEFAULT_REFERENCE);
        transactionStatus.setChannel(channel);
        return transactionStatus;
    }

}
