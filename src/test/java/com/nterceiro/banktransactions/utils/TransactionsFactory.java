package com.nterceiro.banktransactions.utils;

import com.nterceiro.banktransactions.model.Transaction;

import java.time.LocalDateTime;

public class TransactionsFactory {

    public static final Double NEGATIVE_VERY_SHORT_AMOUNT = -10.0;
    public static final Double NEGATIVE_SHORT_AMOUNT = -50.0;
    public static final Double NEGATIVE_LONG_AMOUNT = -180.0;
    public static final Double NEGATIVE_VERY_LONG_AMOUNT = -250.0;
    public static final Double POSITIVE_SHORT_AMOUNT = 10.0;
    public static final Double POSITIVE_LONG_AMOUNT = 100.0;
    public static final Double DEFAULT_FEE = 25.0;
    public static final String DEFAULT_REFERENCE = "12345A";
    public static final String NOT_REGISTERED_REFERENCE = "not registered reference";

    public static Transaction buildSimpleTransaction(double amount, String account) {
        Transaction transaction = new Transaction();
        transaction.setAccountIban(account);
        transaction.setAmount(amount);
        return transaction;
    }

    public static Transaction buildCompleteTransaction(double amount, String account) {
        LocalDateTime localDateTime = LocalDateTime.of(2020, 8, 27, 12, 00);
        return buildCompleteTransaction(DEFAULT_REFERENCE, amount, account, localDateTime);
    }

    public static Transaction buildCompleteTransaction(double amount, String account, Date date) {
        switch (date) {
            case YESTERDAY:
                return buildCompleteTransaction(DEFAULT_REFERENCE, amount, account, LocalDateTime.now().minusDays(1));
            case TOMORROW:
                return buildCompleteTransaction(DEFAULT_REFERENCE, amount, account, LocalDateTime.now().plusDays(1));
            default:
                return buildCompleteTransaction(DEFAULT_REFERENCE, amount, account, LocalDateTime.now());
        }
    }

    private static Transaction buildCompleteTransaction(String reference, double amount, String account,
                                                        LocalDateTime localDateTime) {
        Transaction transaction = new Transaction();
        transaction.setReference(reference);
        transaction.setAccountIban(account);
        transaction.setDate(localDateTime);
        transaction.setAmount(amount);
        transaction.setFee(DEFAULT_FEE);
        transaction.setDescription("Complete transaction");
        return transaction;
    }

    public enum Date {
        YESTERDAY, TODAY, TOMORROW
    }

}
