package com.nterceiro.banktransactions.integration;

import com.nterceiro.banktransactions.BankTransactionsApplication;
import com.nterceiro.banktransactions.dao.AccountEntity;
import com.nterceiro.banktransactions.dao.AccountsRepository;
import com.nterceiro.banktransactions.model.Account;
import com.nterceiro.banktransactions.service.AccountsService;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static com.nterceiro.banktransactions.utils.AccountsFactory.buildAccountWithCash;
import static com.nterceiro.banktransactions.utils.AccountsFactory.buildAccountWithoutCash;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BankTransactionsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class AccountsServiceIntegrationTest {

    @Autowired
    private AccountsService service;
    @Autowired
    private AccountsRepository repository;
    @Autowired
    private ModelMapper modelMapper;

    @After
    public void tearDown() {
        repository.deleteAll();
    }

    @Test
    public void testFindAll() {
        Account account1 = buildAccountWithCash();
        Account account2 = buildAccountWithoutCash();
        repository.save(modelMapper.map(account1, AccountEntity.class));
        repository.save(modelMapper.map(account2, AccountEntity.class));
        List<Account> accountList = service.findAll();

        assertFalse(accountList.isEmpty());
        assertTrue(accountList.contains(account1));
        assertTrue(accountList.contains(account2));
    }

    @Test
    public void testFindAllEmpty() {
        assertTrue(service.findAll().isEmpty());
    }

}
