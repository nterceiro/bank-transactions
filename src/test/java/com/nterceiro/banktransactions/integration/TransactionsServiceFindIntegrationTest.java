package com.nterceiro.banktransactions.integration;

import com.nterceiro.banktransactions.BankTransactionsApplication;
import com.nterceiro.banktransactions.dao.TransactionsRepository;
import com.nterceiro.banktransactions.model.Transaction;
import com.nterceiro.banktransactions.service.TransactionsService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static com.nterceiro.banktransactions.model.Sort.*;
import static com.nterceiro.banktransactions.utils.AccountsFactory.ACCOUNT_WITHOUT_CASH;
import static com.nterceiro.banktransactions.utils.AccountsFactory.ACCOUNT_WITH_CASH;
import static com.nterceiro.banktransactions.utils.TransactionsFactory.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BankTransactionsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class TransactionsServiceFindIntegrationTest {

    private Transaction positiveLongTransactionUser1;
    private Transaction positiveShortTransactionUser1;
    private Transaction negativeShortTransactionUser2;
    private Transaction negativeLongTransactionUser2;

    @Autowired
    private TransactionsService service;
    @Autowired
    private TransactionsRepository repository;

    @Before
    public void setUp() {
        positiveLongTransactionUser1 = service.save(buildSimpleTransaction(POSITIVE_LONG_AMOUNT, ACCOUNT_WITH_CASH));
        positiveShortTransactionUser1 = service.save(buildSimpleTransaction(POSITIVE_SHORT_AMOUNT, ACCOUNT_WITH_CASH));
        negativeShortTransactionUser2 = service.save(buildCompleteTransaction(NEGATIVE_VERY_SHORT_AMOUNT, ACCOUNT_WITHOUT_CASH));
        negativeLongTransactionUser2 = service.save(buildCompleteTransaction(NEGATIVE_LONG_AMOUNT, ACCOUNT_WITHOUT_CASH));
    }

    @After
    public void tearDown() {
        repository.deleteAll();
    }

    @Test
    public void testFindAll() {
        List<Transaction> transactions = service.findTransactions(null, NONE);

        assertEquals(4, transactions.size());
        assertTrue(transactions.contains(positiveLongTransactionUser1));
        assertTrue(transactions.contains(positiveShortTransactionUser1));
        assertTrue(transactions.contains(negativeShortTransactionUser2));
        assertTrue(transactions.contains(negativeLongTransactionUser2));
    }

    @Test
    public void testFindAllByIban() {
        List<Transaction> transactions = service.findTransactions(ACCOUNT_WITH_CASH, NONE);

        assertEquals(2, transactions.size());
        assertTrue(transactions.contains(positiveLongTransactionUser1));
        assertTrue(transactions.contains(positiveShortTransactionUser1));
    }

    @Test
    public void testFindAllByNonExistentIban() {
        List<Transaction> transactions = service.findTransactions("NewOne", NONE);

        assertTrue(transactions.isEmpty());
    }

    @Test
    public void testFindAllAndSortNone() {
        List<Transaction> transactions = service.findTransactions(null, NONE);

        assertEquals(4, transactions.size());
        assertTrue(transactions.contains(positiveLongTransactionUser1));
        assertTrue(transactions.contains(positiveShortTransactionUser1));
        assertTrue(transactions.contains(negativeShortTransactionUser2));
        assertTrue(transactions.contains(negativeLongTransactionUser2));
    }

    @Test
    public void testFindAllAndSortAscending() {
        List<Transaction> transactions = service.findTransactions(null, ASCENDING);

        assertEquals(4, transactions.size());
        assertEquals(negativeLongTransactionUser2, transactions.get(0));
        assertEquals(negativeShortTransactionUser2, transactions.get(1));
        assertEquals(positiveShortTransactionUser1, transactions.get(2));
        assertEquals(positiveLongTransactionUser1, transactions.get(3));
    }

    @Test
    public void testFindAllAndSortDescending() {
        List<Transaction> transactions = service.findTransactions(null, DESCENDING);

        assertEquals(4, transactions.size());
        assertEquals(positiveLongTransactionUser1, transactions.get(0));
        assertEquals(positiveShortTransactionUser1, transactions.get(1));
        assertEquals(negativeShortTransactionUser2, transactions.get(2));
        assertEquals(negativeLongTransactionUser2, transactions.get(3));
    }

    @Test
    public void testFindByIbanAndSortAscending() {
        List<Transaction> transactions = service.findTransactions(ACCOUNT_WITH_CASH, ASCENDING);

        assertEquals(2, transactions.size());
        assertEquals(positiveShortTransactionUser1, transactions.get(0));
        assertEquals(positiveLongTransactionUser1, transactions.get(1));
    }

    @Test
    public void testFindByIbanAndSortDescending() {
        List<Transaction> transactions = service.findTransactions(ACCOUNT_WITHOUT_CASH, DESCENDING);

        assertEquals(2, transactions.size());
        assertEquals(negativeShortTransactionUser2, transactions.get(0));
        assertEquals(negativeLongTransactionUser2, transactions.get(1));
    }

}
