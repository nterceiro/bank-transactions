package com.nterceiro.banktransactions.integration;

import com.nterceiro.banktransactions.BankTransactionsApplication;
import com.nterceiro.banktransactions.dao.AccountEntity;
import com.nterceiro.banktransactions.dao.AccountsRepository;
import com.nterceiro.banktransactions.dao.TransactionEntity;
import com.nterceiro.banktransactions.dao.TransactionsRepository;
import com.nterceiro.banktransactions.exception.TransactionException;
import com.nterceiro.banktransactions.model.Transaction;
import com.nterceiro.banktransactions.service.TransactionsService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static com.nterceiro.banktransactions.utils.AccountsFactory.*;
import static com.nterceiro.banktransactions.utils.ComparatorModelEntity.assertEqualWithoutId;
import static com.nterceiro.banktransactions.utils.TransactionsFactory.*;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BankTransactionsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class TransactionsServiceIntegrationTest {

    @Autowired
    private TransactionsService service;
    @Autowired
    private TransactionsRepository transactionsRepository;
    @Autowired
    private AccountsRepository accountsRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Before
    public void setUp() {
        accountsRepository.save(modelMapper.map(buildAccountWithCash(), AccountEntity.class));
    }

    @After
    public void tearDown() {
        accountsRepository.deleteAll();
        transactionsRepository.deleteAll();
    }

    @Test
    public void testSaveTransaction() {
        Transaction transactionWithoutReference = buildSimpleTransaction(POSITIVE_SHORT_AMOUNT, ACCOUNT_WITHOUT_CASH);
        transactionWithoutReference.setReference(null);
        transactionWithoutReference = service.save(transactionWithoutReference);
        Transaction transactionWithReference = service.save(buildCompleteTransaction(POSITIVE_SHORT_AMOUNT, ACCOUNT_WITHOUT_CASH));

        assertNotNull(transactionWithoutReference.getReference());
        assertEquals(DEFAULT_REFERENCE, transactionWithReference.getReference());
    }

    @Test
    public void testNonExistentAccountWithPositiveTransaction() throws TransactionException {
        assertNull(accountsRepository.findByAccountIban(ACCOUNT_WITHOUT_CASH));

        Transaction transaction = buildSimpleTransaction(POSITIVE_SHORT_AMOUNT, ACCOUNT_WITHOUT_CASH);
        service.applyTransaction(transaction);
        List<TransactionEntity> transactions = transactionsRepository.findAll();
        AccountEntity account = accountsRepository.findByAccountIban(ACCOUNT_WITHOUT_CASH);

        assertTrue(assertEqualWithoutId(transaction, transactions.get(0)));
        assertNotNull(account);
        assertEquals(POSITIVE_SHORT_AMOUNT, account.getAmount());
    }

    @Test
    public void testNonExistentAccountWithNegativeTransaction() {
        assertNull(accountsRepository.findByAccountIban(ACCOUNT_WITHOUT_CASH));
        try {
            service.applyTransaction(buildSimpleTransaction(NEGATIVE_VERY_SHORT_AMOUNT, ACCOUNT_WITHOUT_CASH));
            fail();
        } catch (TransactionException e) {
            assertTrue(transactionsRepository.findAll().isEmpty());
            assertNull(accountsRepository.findByAccountIban(ACCOUNT_WITHOUT_CASH));
        }
    }

    @Test
    public void testNonExistentAccountWithFeeButPositiveAmount() throws TransactionException {
        assertNull(accountsRepository.findByAccountIban(ACCOUNT_WITHOUT_CASH));

        Transaction transaction = buildCompleteTransaction(POSITIVE_LONG_AMOUNT, ACCOUNT_WITHOUT_CASH);
        service.applyTransaction(transaction);
        List<TransactionEntity> transactions = transactionsRepository.findAll();
        AccountEntity account = accountsRepository.findByAccountIban(ACCOUNT_WITHOUT_CASH);
        Double expectedCash = POSITIVE_LONG_AMOUNT - DEFAULT_FEE;

        assertTrue(assertEqualWithoutId(transaction, transactions.get(0)));
        assertNotNull(account);
        assertEquals(expectedCash, account.getAmount());
    }

    @Test
    public void testNonExistentAccountWithFeeAndNegativeAmountResult() {
        assertNull(accountsRepository.findByAccountIban(ACCOUNT_WITHOUT_CASH));
        try {
            service.applyTransaction(buildCompleteTransaction(NEGATIVE_VERY_SHORT_AMOUNT, ACCOUNT_WITHOUT_CASH));
            fail();
        } catch (TransactionException e) {
            assertTrue(transactionsRepository.findAll().isEmpty());
            assertNull(accountsRepository.findByAccountIban(ACCOUNT_WITHOUT_CASH));
        }
    }

    @Test
    public void testExistentAccountWithPositiveTransaction() throws TransactionException {
        Transaction transaction = buildSimpleTransaction(POSITIVE_SHORT_AMOUNT, ACCOUNT_WITH_CASH);
        service.applyTransaction(transaction);
        List<TransactionEntity> transactions = transactionsRepository.findAll();
        AccountEntity account = accountsRepository.findByAccountIban(ACCOUNT_WITH_CASH);
        Double expectedCash = POSITIVE_SHORT_AMOUNT + INITIAL_CASH;

        assertTrue(assertEqualWithoutId(transaction, transactions.get(0)));
        assertEquals(expectedCash, account.getAmount());
    }

    @Test
    public void testExistentAccountWithNegativeTransactionButPositiveAmount() throws TransactionException {
        Transaction transaction = buildSimpleTransaction(NEGATIVE_VERY_SHORT_AMOUNT, ACCOUNT_WITH_CASH);
        service.applyTransaction(transaction);
        List<TransactionEntity> transactions = transactionsRepository.findAll();
        AccountEntity account = accountsRepository.findByAccountIban(ACCOUNT_WITH_CASH);
        Double expectedCash = NEGATIVE_VERY_SHORT_AMOUNT + INITIAL_CASH;

        assertTrue(assertEqualWithoutId(transaction, transactions.get(0)));
        assertEquals(expectedCash, account.getAmount());
    }

    @Test
    public void testExistentAccountWithNegativeTransactionButNegativeAmount() {
        try {
            service.applyTransaction(buildSimpleTransaction(NEGATIVE_VERY_LONG_AMOUNT, ACCOUNT_WITH_CASH));
            fail();
        } catch (TransactionException e) {
            AccountEntity account = accountsRepository.findByAccountIban(ACCOUNT_WITH_CASH);
            assertTrue(transactionsRepository.findAll().isEmpty());
            assertEquals(INITIAL_CASH, account.getAmount());
        }
    }

    @Test
    public void testExistentAccountWithFeeButPositiveAmount() throws TransactionException {
        Transaction transaction = buildCompleteTransaction(POSITIVE_LONG_AMOUNT, ACCOUNT_WITH_CASH);
        service.applyTransaction(transaction);
        List<TransactionEntity> transactions = transactionsRepository.findAll();
        AccountEntity account = accountsRepository.findByAccountIban(ACCOUNT_WITH_CASH);
        Double expectedCash = INITIAL_CASH + POSITIVE_LONG_AMOUNT - DEFAULT_FEE;

        assertTrue(assertEqualWithoutId(transaction, transactions.get(0)));
        assertEquals(expectedCash, account.getAmount());
    }

    @Test
    public void testExistentAccountWithFeeAndNegativeAmountButPositiveResult() throws TransactionException {
        Transaction transaction = buildCompleteTransaction(NEGATIVE_VERY_SHORT_AMOUNT, ACCOUNT_WITH_CASH);
        service.applyTransaction(transaction);
        List<TransactionEntity> transactions = transactionsRepository.findAll();
        AccountEntity account = accountsRepository.findByAccountIban(ACCOUNT_WITH_CASH);
        Double expectedCash = INITIAL_CASH + NEGATIVE_VERY_SHORT_AMOUNT - DEFAULT_FEE;

        assertTrue(assertEqualWithoutId(transaction, transactions.get(0)));
        assertEquals(expectedCash, account.getAmount());
    }

    @Test
    public void testExistentAccountWithFeeAndNegativeAmountAndNegativeResult() {
        try {
            service.applyTransaction(buildCompleteTransaction(NEGATIVE_LONG_AMOUNT, ACCOUNT_WITH_CASH));
            fail();
        } catch (TransactionException e) {
            AccountEntity account = accountsRepository.findByAccountIban(ACCOUNT_WITH_CASH);
            assertTrue(transactionsRepository.findAll().isEmpty());
            assertEquals(INITIAL_CASH, account.getAmount());
        }
    }

}
