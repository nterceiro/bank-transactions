package com.nterceiro.banktransactions.integration;

import com.nterceiro.banktransactions.BankTransactionsApplication;
import com.nterceiro.banktransactions.dao.TransactionsRepository;
import com.nterceiro.banktransactions.model.Transaction;
import com.nterceiro.banktransactions.model.TransactionResponse;
import com.nterceiro.banktransactions.model.TransactionStatus;
import com.nterceiro.banktransactions.service.TransactionsService;
import com.nterceiro.banktransactions.service.TransactionsStatusService;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static com.nterceiro.banktransactions.model.Channel.*;
import static com.nterceiro.banktransactions.model.Status.*;
import static com.nterceiro.banktransactions.utils.AccountsFactory.ACCOUNT_WITH_CASH;
import static com.nterceiro.banktransactions.utils.TransactionStatusFactory.buildInvalidTransactionStatus;
import static com.nterceiro.banktransactions.utils.TransactionStatusFactory.buildValidTransactionStatus;
import static com.nterceiro.banktransactions.utils.TransactionsFactory.*;
import static com.nterceiro.banktransactions.utils.TransactionsFactory.Date.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BankTransactionsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class TransactionsStatusServiceIntegrationTest {

    @Autowired
    private TransactionsRepository repository;
    @Autowired
    private TransactionsStatusService service;
    @Autowired
    private TransactionsService transactionsService;

    @After
    public void tearDown() {
        repository.deleteAll();
    }

    @Test
    public void testInvalid() {
        TransactionResponse response = service.checkStatus(buildInvalidTransactionStatus());

        TransactionResponse expectedResponse = TransactionResponse.builder()
                .reference(NOT_REGISTERED_REFERENCE)
                .status(INVALID)
                .build();

        assertEquals(expectedResponse, response);
    }

    @Test
    public void testSimpleTransaction() {
        Transaction transaction = transactionsService.save(buildSimpleTransaction(NEGATIVE_LONG_AMOUNT, ACCOUNT_WITH_CASH));

        TransactionStatus transactionStatus = new TransactionStatus();
        transactionStatus.setReference(transaction.getReference());
        transactionStatus.setChannel(CLIENT);

        TransactionResponse response = service.checkStatus(transactionStatus);

        TransactionResponse expectedResponse = TransactionResponse.builder()
                .reference(transaction.getReference())
                .status(INVALID)
                .build();

        assertEquals(expectedResponse, response);
    }

    @Test
    public void testCompleteTransactionFromClientDoneYesterday() {
        transactionsService.save(buildCompleteTransaction(POSITIVE_SHORT_AMOUNT, ACCOUNT_WITH_CASH, YESTERDAY));

        TransactionResponse response = service.checkStatus(buildValidTransactionStatus(CLIENT));

        TransactionResponse expectedResponse = TransactionResponse.builder()
                .reference(DEFAULT_REFERENCE)
                .status(SETTLED)
                .amount(POSITIVE_SHORT_AMOUNT - DEFAULT_FEE)
                .build();

        assertEquals(expectedResponse, response);
    }

    @Test
    public void testCompleteTransactionFromClientDoneToday() {
        transactionsService.save(buildCompleteTransaction(POSITIVE_LONG_AMOUNT, ACCOUNT_WITH_CASH, TODAY));

        TransactionResponse response = service.checkStatus(buildValidTransactionStatus(CLIENT));

        TransactionResponse expectedResponse = TransactionResponse.builder()
                .reference(DEFAULT_REFERENCE)
                .status(PENDING)
                .amount(POSITIVE_LONG_AMOUNT - DEFAULT_FEE)
                .build();

        assertEquals(expectedResponse, response);
    }

    @Test
    public void testCompleteTransactionFromClientDoneTomorrow() {
        transactionsService.save(buildCompleteTransaction(NEGATIVE_SHORT_AMOUNT, ACCOUNT_WITH_CASH, TOMORROW));

        TransactionResponse response = service.checkStatus(buildValidTransactionStatus(CLIENT));

        TransactionResponse expectedResponse = TransactionResponse.builder()
                .reference(DEFAULT_REFERENCE)
                .status(FUTURE)
                .amount(NEGATIVE_SHORT_AMOUNT - DEFAULT_FEE)
                .build();

        assertEquals(expectedResponse, response);
    }

    @Test
    public void testCompleteTransactionFromClientWithoutFee() {
        Transaction transaction = buildCompleteTransaction(POSITIVE_LONG_AMOUNT, ACCOUNT_WITH_CASH, TOMORROW);
        transaction.setFee(null);
        transactionsService.save(transaction);

        TransactionResponse response = service.checkStatus(buildValidTransactionStatus(CLIENT));

        TransactionResponse expectedResponse = TransactionResponse.builder()
                .reference(DEFAULT_REFERENCE)
                .status(FUTURE)
                .amount(POSITIVE_LONG_AMOUNT)
                .build();

        assertEquals(expectedResponse, response);
    }

    @Test
    public void testCompleteTransactionFromAtmDoneYesterday() {
        transactionsService.save(buildCompleteTransaction(POSITIVE_SHORT_AMOUNT, ACCOUNT_WITH_CASH, YESTERDAY));

        TransactionResponse response = service.checkStatus(buildValidTransactionStatus(ATM));

        TransactionResponse expectedResponse = TransactionResponse.builder()
                .reference(DEFAULT_REFERENCE)
                .status(SETTLED)
                .amount(POSITIVE_SHORT_AMOUNT - DEFAULT_FEE)
                .build();

        assertEquals(expectedResponse, response);
    }

    @Test
    public void testCompleteTransactionFromAtmDoneToday() {
        transactionsService.save(buildCompleteTransaction(POSITIVE_LONG_AMOUNT, ACCOUNT_WITH_CASH, TODAY));

        TransactionResponse response = service.checkStatus(buildValidTransactionStatus(ATM));

        TransactionResponse expectedResponse = TransactionResponse.builder()
                .reference(DEFAULT_REFERENCE)
                .status(PENDING)
                .amount(POSITIVE_LONG_AMOUNT - DEFAULT_FEE)
                .build();

        assertEquals(expectedResponse, response);
    }

    @Test
    public void testCompleteTransactionFromAtmDoneTomorrow() {
        transactionsService.save(buildCompleteTransaction(NEGATIVE_SHORT_AMOUNT, ACCOUNT_WITH_CASH, TOMORROW));

        TransactionResponse response = service.checkStatus(buildValidTransactionStatus(ATM));

        TransactionResponse expectedResponse = TransactionResponse.builder()
                .reference(DEFAULT_REFERENCE)
                .status(PENDING)
                .amount(NEGATIVE_SHORT_AMOUNT - DEFAULT_FEE)
                .build();

        assertEquals(expectedResponse, response);
    }

    @Test
    public void testCompleteTransactionFromAtmWithoutFee() {
        Transaction transaction = buildCompleteTransaction(POSITIVE_LONG_AMOUNT, ACCOUNT_WITH_CASH, TOMORROW);
        transaction.setFee(null);
        transactionsService.save(transaction);

        TransactionResponse response = service.checkStatus(buildValidTransactionStatus(ATM));

        TransactionResponse expectedResponse = TransactionResponse.builder()
                .reference(DEFAULT_REFERENCE)
                .status(PENDING)
                .amount(POSITIVE_LONG_AMOUNT)
                .build();

        assertEquals(expectedResponse, response);
    }

    @Test
    public void testCompleteTransactionFromInternalDoneYesterday() {
        transactionsService.save(buildCompleteTransaction(POSITIVE_SHORT_AMOUNT, ACCOUNT_WITH_CASH, YESTERDAY));

        TransactionResponse response = service.checkStatus(buildValidTransactionStatus(INTERNAL));

        TransactionResponse expectedResponse = TransactionResponse.builder()
                .reference(DEFAULT_REFERENCE)
                .status(SETTLED)
                .amount(POSITIVE_SHORT_AMOUNT)
                .fee(DEFAULT_FEE)
                .build();

        assertEquals(expectedResponse, response);
    }

    @Test
    public void testCompleteTransactionFromInternalDoneToday() {
        transactionsService.save(buildCompleteTransaction(POSITIVE_LONG_AMOUNT, ACCOUNT_WITH_CASH, TODAY));

        TransactionResponse response = service.checkStatus(buildValidTransactionStatus(INTERNAL));

        TransactionResponse expectedResponse = TransactionResponse.builder()
                .reference(DEFAULT_REFERENCE)
                .status(PENDING)
                .amount(POSITIVE_LONG_AMOUNT)
                .fee(DEFAULT_FEE)
                .build();

        assertEquals(expectedResponse, response);
    }

    @Test
    public void testCompleteTransactionFromInternalDoneTomorrow() {
        transactionsService.save(buildCompleteTransaction(NEGATIVE_SHORT_AMOUNT, ACCOUNT_WITH_CASH, TOMORROW));

        TransactionResponse response = service.checkStatus(buildValidTransactionStatus(INTERNAL));

        TransactionResponse expectedResponse = TransactionResponse.builder()
                .reference(DEFAULT_REFERENCE)
                .status(FUTURE)
                .amount(NEGATIVE_SHORT_AMOUNT)
                .fee(DEFAULT_FEE)
                .build();

        assertEquals(expectedResponse, response);
    }

    @Test
    public void testCompleteTransactionFromInternalWithoutFee() {
        Transaction transaction = buildCompleteTransaction(NEGATIVE_SHORT_AMOUNT, ACCOUNT_WITH_CASH, TOMORROW);
        transaction.setFee(null);
        transactionsService.save(transaction);

        TransactionResponse response = service.checkStatus(buildValidTransactionStatus(INTERNAL));

        TransactionResponse expectedResponse = TransactionResponse.builder()
                .reference(DEFAULT_REFERENCE)
                .status(FUTURE)
                .amount(NEGATIVE_SHORT_AMOUNT)
                .fee(0.0)
                .build();

        assertEquals(expectedResponse, response);
    }

}
