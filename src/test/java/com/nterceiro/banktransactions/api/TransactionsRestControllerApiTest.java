package com.nterceiro.banktransactions.api;

import com.nterceiro.banktransactions.BankTransactionsApplication;
import com.nterceiro.banktransactions.dao.AccountEntity;
import com.nterceiro.banktransactions.dao.AccountsRepository;
import com.nterceiro.banktransactions.dao.TransactionEntity;
import com.nterceiro.banktransactions.dao.TransactionsRepository;
import com.nterceiro.banktransactions.model.*;
import com.nterceiro.banktransactions.service.TransactionsService;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static com.nterceiro.banktransactions.model.Channel.*;
import static com.nterceiro.banktransactions.model.Status.*;
import static com.nterceiro.banktransactions.utils.AccountsFactory.*;
import static com.nterceiro.banktransactions.utils.TransactionStatusFactory.buildInvalidTransactionStatus;
import static com.nterceiro.banktransactions.utils.TransactionStatusFactory.buildValidTransactionStatus;
import static com.nterceiro.banktransactions.utils.TransactionsFactory.*;
import static com.nterceiro.banktransactions.utils.TransactionsFactory.Date.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BankTransactionsApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class TransactionsRestControllerApiTest {

    private RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private TransactionsRepository transactionsRepository;
    @Autowired
    private AccountsRepository accountsRepository;
    @Autowired
    private TransactionsService service;
    @Autowired
    private ModelMapper modelMapper;

    @Value("${local.server.port}")
    private int port;

    @After
    public void tearDown() {
        transactionsRepository.deleteAll();
        accountsRepository.deleteAll();
    }

    @Test
    public void testCreateTransaction() {
        HttpEntity<Transaction> entity = new HttpEntity<>(buildSimpleTransaction(POSITIVE_SHORT_AMOUNT, ACCOUNT_WITHOUT_CASH));
        ResponseEntity response = restTemplate.exchange(buildBasicUrl() + "/transaction", HttpMethod.POST, entity, Void.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test(expected = HttpClientErrorException.class)
    public void testCreateTransactionWithError() {
        HttpEntity<Transaction> entity = new HttpEntity<>(buildSimpleTransaction(NEGATIVE_VERY_SHORT_AMOUNT, ACCOUNT_WITHOUT_CASH));
        restTemplate.exchange(buildBasicUrl() + "/transaction", HttpMethod.POST, entity, Void.class);
    }

    @Test
    public void testGetEmptyTransactions() {
        ResponseEntity<Transaction[]> response = restTemplate.getForEntity(buildBasicUrl() + "/transactions", Transaction[].class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(0, response.getBody().length);
    }

    @Test
    public void testGetTransactions() {
        Transaction transaction = buildCompleteTransaction(POSITIVE_SHORT_AMOUNT, ACCOUNT_WITH_CASH);
        service.save(transaction);
        ResponseEntity<Transaction[]> response =
                restTemplate.getForEntity(buildBasicUrl() + "/transactions", Transaction[].class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(1, response.getBody().length);
        assertEquals(transaction, response.getBody()[0]);
    }

    @Test
    public void testGetTransactionsByIbanAndSorted() {
        Transaction transaction1 = buildCompleteTransaction(POSITIVE_SHORT_AMOUNT, ACCOUNT_WITH_CASH);
        Transaction transaction2 = buildCompleteTransaction(NEGATIVE_SHORT_AMOUNT, ACCOUNT_WITH_CASH);
        service.save(transaction1);
        service.save(transaction2);
        service.save(buildCompleteTransaction(NEGATIVE_VERY_LONG_AMOUNT, ACCOUNT_WITHOUT_CASH));
        String url = buildBasicUrl() + "/transactions?iban=" + ACCOUNT_WITH_CASH + "&sort=" + Sort.ASCENDING.name();
        ResponseEntity<Transaction[]> response = restTemplate.getForEntity(url, Transaction[].class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(2, response.getBody().length);
        assertEquals(transaction2, response.getBody()[0]);
        assertEquals(transaction1, response.getBody()[1]);
    }

    @Test(expected = HttpClientErrorException.class)
    public void testGetTransactionsWithWrongSort() {
        String url = buildBasicUrl() + "/transactions?sort=RANDOM";
        restTemplate.getForEntity(url, TransactionEntity[].class);
    }

    @Test
    public void testGetEmptyAccounts() {
        ResponseEntity<Account[]> response = restTemplate.getForEntity(buildBasicUrl() + "/accounts", Account[].class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(0, response.getBody().length);
    }

    @Test
    public void testGetAccounts() {
        Account account = buildAccountWithCash();
        accountsRepository.save(modelMapper.map(account, AccountEntity.class));
        ResponseEntity<Account[]> response = restTemplate.getForEntity(buildBasicUrl() + "/accounts", Account[].class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(1, response.getBody().length);
        assertEquals(account, response.getBody()[0]);
    }

    @Test
    public void testCheckStatusClient() {
        Transaction transaction = buildCompleteTransaction(POSITIVE_SHORT_AMOUNT, ACCOUNT_WITH_CASH, YESTERDAY);
        service.save(transaction);
        HttpEntity<TransactionStatus> httpEntity = new HttpEntity<>(buildValidTransactionStatus(CLIENT));
        ResponseEntity<TransactionResponse> response =
                restTemplate.exchange(buildBasicUrl() + "/status", HttpMethod.POST, httpEntity, TransactionResponse.class);

        TransactionResponse expectedResponse = TransactionResponse.builder()
                .reference(DEFAULT_REFERENCE)
                .status(SETTLED)
                .amount(POSITIVE_SHORT_AMOUNT - DEFAULT_FEE)
                .build();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedResponse, response.getBody());
    }

    @Test
    public void testCheckStatusAtm() {
        Transaction transaction = buildCompleteTransaction(POSITIVE_LONG_AMOUNT, ACCOUNT_WITH_CASH, TODAY);
        service.save(transaction);
        HttpEntity<TransactionStatus> httpEntity = new HttpEntity<>(buildValidTransactionStatus(ATM));
        ResponseEntity<TransactionResponse> response =
                restTemplate.exchange(buildBasicUrl() + "/status", HttpMethod.POST, httpEntity, TransactionResponse.class);

        TransactionResponse expectedResponse = TransactionResponse.builder()
                .reference(DEFAULT_REFERENCE)
                .status(PENDING)
                .amount(POSITIVE_LONG_AMOUNT - DEFAULT_FEE)
                .build();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedResponse, response.getBody());
    }

    @Test
    public void testCheckStatusInternal() {
        Transaction transaction = buildCompleteTransaction(NEGATIVE_SHORT_AMOUNT, ACCOUNT_WITH_CASH, TOMORROW);
        service.save(transaction);
        HttpEntity<TransactionStatus> httpEntity = new HttpEntity<>(buildValidTransactionStatus(INTERNAL));
        ResponseEntity<TransactionResponse> response =
                restTemplate.exchange(buildBasicUrl() + "/status", HttpMethod.POST, httpEntity, TransactionResponse.class);

        TransactionResponse expectedResponse = TransactionResponse.builder()
                .reference(DEFAULT_REFERENCE)
                .status(FUTURE)
                .amount(NEGATIVE_SHORT_AMOUNT)
                .fee(DEFAULT_FEE)
                .build();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedResponse, response.getBody());
    }

    @Test
    public void testCheckStatusError() {
        HttpEntity<TransactionStatus> entity = new HttpEntity<>(buildInvalidTransactionStatus());
        ResponseEntity<TransactionResponse> response =
                restTemplate.exchange(buildBasicUrl() + "/status", HttpMethod.POST, entity, TransactionResponse.class);

        TransactionResponse expectedResponse = TransactionResponse.builder()
                .reference(NOT_REGISTERED_REFERENCE)
                .status(INVALID)
                .build();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedResponse, response.getBody());
    }

    private String buildBasicUrl() {
        return "http://localhost:" + port + "/rest";
    }

}
