# Getting Started

## How to run

Open BankTransactionsApp class and run it or the main method and set VM Options with `-Dspring.config.location=src/test/resources/test.properties`
The app will be available at `http://localhost:8080` and the available urls are:
* GET http://localhost:8080/rest/acounts to see all accounts
* GET http://localhost:8080/rest/transactions?iban=ES12&sort=NONE to see stored transactions. Parameters are optional
    * `iban` accounts iban
    * `sort` with values `NONE`, `ASCENDING` or `DESCENDING`
* POST http://localhost:8080/rest/transaction with a JSON body like
```
{
   "reference":"12345A",
   "account_iban":"ES9820385778983000760236",
   "date":"2019-07-16T16:55:42.000Z",
   "amount":193.38,
   "fee":3.18,
   "description":"Restaurant payment"
}
```
* POST http://localhost:8080/rest/status with a JSON body like
    * `reference` the transaction's reference
    * `channel` with values `CLIENT`, `ATM` or `INTERNAL`
```
{
   "reference":"12345A",
   "channel":"INTERNAL"
}
```

## How to compile with Lombok

In IntelliJ, go to File > Settings > Build, Execution, Deployment > Compiler > Annotation Processors and check Enable annotation processing

## SonarQube

There is a gradle task that analyzes the project with SonarQube. It is currently analyzed with a dockerized SonarQube, but it can be changed to any SonarQube url updating the variable `sonar_base_url` in gradle.properties.
This analysis is optional.
